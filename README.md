# Smart Home Server
Server for a smart-home-application. Developed together with [this app](https://bitbucket.org/luggikraft/ionic-app/src/master/).

__Features__:

- Saving and evaluating sensor measurements
- Sending push notifications

### Install:

- [NodeJS](https://github.com/nodesource/distributions/blob/master/README.md#debinstall) minimal version 12.x

  sudo apt install mongodb
and

	sudo npm i -g nodemon
	sudo npm i -g parse-dashboard

### Local requirements:
Install local requirements:

	npm i
	
### Run server
1.) Start MongoDB
2.) Start the server

		node cloud/main.js

	- The parse-dashboad is automatically started with the server.

### Configuration:
If the server is running in deploy ("on a real server") in `parse-dashboard-config` the global ip (domain with subdomain) must be entered and not the localhost.
- To automatically renew letsencrypt certificates cp `update_https.sh` to /etc/cron.weekly. [Not verified]

- `parse-config.json`:

- `server-config.json`:

- `parse-dashboard-config.json`:
  - If the server is running in deploy ("on a real server") in `parse-dashboard-config.json` the global ip (domain with subdomain) must be entered and not the localhost.
  
	   "serverURL": "https://greenthumb-api.ludwigel.de/parse",

  - Passwort entry can be generated on [bcrypt-generator.com](https://bcrypt-generator.com/)
  
### Deploy

Automatically start mongodb at startup:

  systemctl enable mongod.service
  
Create configuration file for `pm2` to run at startup with 

  pm2 startup
  
Start the application 

  pm2 start deploy/greenthumb-test.config.js
  
and save that app, so that pm2 is automattically restarting app.js after restart.

  pm2 save

You can check if the server is running with `pm2 list`. However this command is user dependend. So it will only show the process if the current user is running the process.


  
  
