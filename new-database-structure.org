#+TITLE: New Database Structure




| Class   | User                | Enduser   | Board           | Measurement | Sensor           |
|---------+---------------------+-----------+-----------------+-------------+------------------|
| Entries | name                | userid    | userid          | sensorid    | name             |
| ...     | password            | pushtoken | subscribedUsers | value       | upperBound       |
|         | email               |           |                 |             | lowerBound       |
|         | emailVerified***    |           |                 |             | unit             |
|         | verified            |           |                 |             | maxNumWarnings   |
|         | is_board            |           |                 |             | lastNotified     |
|         | settings (*enduser) |           |                 |             | tonotifiedUsers* |
|         | config  (*board)    |           |                 |             | subscribedUsers* |
|         |                     |           |                 |             | type**           |
|         |                     |           |                 |             | index**          |
|         |                     |           |                 |             | boardid**        |
|         |                     |           |                 |             |                  |

\*: Do not simple return to user?? tonotifiedUsers --> notifiedUsers
\**: As identifier
\***: basically a cpoy of verified vice verse. Only usefull for the case that emailVerified users shall not be verfied automatically.

** Problems

There is a unclearness in sensor configuration some values like name, upperBound, lowerBound, unit, maxNumWarnings, .. are sensor specific data which cannot converge between different users.
However, values like tonotifiedUsers and subscribedUsers can converge between different users.
For one, I don't know how the server is handling this currently, and how it should handle it. Secondly, this is not clear in the app and will misguide users.
