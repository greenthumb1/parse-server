
const Parse = require('parse/node');
const jwt = require("jsonwebtoken");
var fs = require('fs');

const SensorSettings = require('../models/sensor-settings');
const checkAuth = require('../check-auth');

module.exports = (app) => {

    app.post('/api/board/register', async (req, res, next) => {
        // Request output:
        console.log(`/api/users/signin> req: ${JSON.stringify(req.body)}`);

        if (req.body.username == null) {
            return res.status(200).json({ status: "Error", message: "User name required" });
        }
        if (req.body.password == null) {
            return res.status(200).json({ status: "Error", message: "Password required" });
        }

        // Handle request:
        let user = new Parse.User();
        user.set("username", req.body.username);
        user.set("password", req.body.password);
        //        user.set("email", board.id.concat("@sensor.dev"));
        user.set("verified", false);
        user.set("settings", null);
        user.set("isboard", true);

        try {
            await user.signUp();
        } catch (e) {
            return res.status(200).json({ status: "Error", message: e.message });
        }

        const Board = Parse.Object.extend("Board");
        let board = new Board();
        board.set("subscribedUsers", []);
        board.set("userid", null);
        await board.save();

        user.set("config", Board.createWithoutData(board.id));
        await user.save(null, { useMasterKey: true });

        board.set("userid", Parse.User.createWithoutData(user.id));
        board.save();

        const token = jwt.sign({ userId: user.id }, server_config['jsonWebTokenKey'], { expiresIn: "10y" }); // Optional add {expiresIn: "1h"}

        //Respond to client:
        console.log(`New board created: ${user.id}`);
        return res.status(201).json({
            userData: {
                status: "Ok",
                message: "Board created",
                token: token,
                boardId: board.id,
                username: user.get("username"),
            }
        });

    });

    app.post('/api/board/login', async (req, res, next) => {
        // Request output:
        return res.status(200).json({ status: "Error", message: "Use '/api/users/login' instead" });
    });

    app.post('/api/board/subscribe', checkAuth, async (req, res, next) => {
        console.log(req.body);
        if (req.userData.user.get("isboard") == undefined || req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Boards cannot subscribe to boards" });
        }
        if (req.body.boardid == undefined) {
            return res.status(200).json({ status: "Error", message: "Which board shall be subscribed?" });
        }

        const Board = Parse.Object.extend("Board");
        boardquery = new Parse.Query(Board);
        boardquery.equalTo("objectId", req.body.boardid);
        let board = await boardquery.first();

        if (board == undefined || board == null) {
            return res.status(200).json({ status: "Error", message: "Board not found" });
        }

        subscribed_users = board.get("subscribedUsers");
        console.log(subscribed_users);
        if (subscribed_users.includes(req.userData.userId)) {
            return res.status(200).json({ status: "Error", message: "User already subscribed" });
        }

        subscribed_users.push(req.userData.userId);
        board.set('subscribedUsers', subscribed_users);
        board.save()
        res.status(200).json({ status: "Ok", message: "Subscribed to board" });
    });

    app.post('/api/board/setup/set', checkAuth, async (req, res, next) => {
        if (req.userData.user.get("isboard") == undefined || req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Only users can change board setup" });
        }
        if (req.body.boardSetup == undefined) {
            return res.status(200).json({ status: "Error", message: "boardSetup missing" });
        }
        if (req.body.boardSetup.boardId == undefined) {
            return res.status(200).json({ status: "Error", message: "boardId undefined." });
        }
        if (req.body.boardSetup.boardName == undefined) {
            return res.status(200).json({ status: "Error", message: "boardName undefined." });
        }

        const Board = Parse.Object.extend("Board");
        boardquery = new Parse.Query(Board);
        boardquery.equalTo("objectId", req.body.boardSetup.boardId);
        let board = await boardquery.first();

        console.log(`boardSetup ${JSON.stringify(req.body.boardSetup)}`);
        board.set("boardName", req.body.boardSetup.boardName);
        board.save();

        return res.status(200).json({ status: "Ok", message: "Board config saved." });
    });



    app.post('/api/boards/setup/get', checkAuth, async (req, res, next) => {
        if (req.userData.user.get("isboard") == undefined || req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Only users can get boards setup" });
        }

        const Board = Parse.Object.extend("Board");
        boardquery = new Parse.Query(Board);
        boardquery.equalTo("subscribedUsers", req.userData.userId);
        let boards = await boardquery.find();

        let board_setups = [];
        for (let i = 0; i < boards.length; i++) {
            const Sensor = Parse.Object.extend("Sensor");
            sensorquery = new Parse.Query(Sensor);
            sensorquery.equalTo("boardid", Board.createWithoutData(boards[i].id));
            sensors = await sensorquery.find();

            let sensors_setup = [];
            for (let j = 0; j < sensors.length; j++) {
                const Measurement = Parse.Object.extend("Measurement");
                measurementquery = new Parse.Query(Measurement);
                measurementquery.equalTo("sensorid", Sensor.createWithoutData(sensors[j].id));
                measurementquery.descending("createdAt");
                const measurement = await measurementquery.first();


                sensor_setup = {
                    sensorId: sensors[j].id,
                    sensorName: sensors[j].get("name"),
                    upperBound: sensors[j].get("upperBound"),
                    lowerBound: sensors[j].get("lowerBound"),
                    value: measurement.get("value"),
                    lastUpdate: measurement.get('createdAt'),
                    unit: sensors[j].get("unit"),
                    maxNumWarnings: sensors[j].get("maxNumWarnings"),
                    subscribed: sensors[j].get("subscribedUsers").includes(req.userData.userId),
                    notified: sensors[j].get("notifiedUsers").includes(req.userData.userId),
                    sensorType: sensors[j].get("type"),
                    sensorIndex: sensors[j].get("index")
                };
                sensors_setup.push(sensor_setup);
            }
            const User = Parse.Object.extend("User");
            userquery = new Parse.Query(User);
            userquery.equalTo("objectId", boards[i].get("userid").id);
            user = await userquery.first();

            board_setup = {
                boardId: boards[i].id,
                boardName: boards[i].get("boardName"),
                sensorSetups: sensors_setup
            };
            board_setups.push(board_setup);
        }

        res.status(200).json({ status: "Ok", message: "Boards setup attached", boardSetups: board_setups });
    });
}
