const Parse = require('parse/node');

const checkAuth = require('../check-auth');
const sensorHelper = require('../sensor-helper');

module.exports = (app) => {

    app.post('/api/sensor/subscribe', checkAuth, async (req, res, next) => {
        console.log(req.body);
        if (req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Boards cannot subscribe to sensors" });
        }
        if (req.body.sensorid == undefined) {
            return res.status(200).json({ status: "Error", message: "Which sensor shall be subscribed?" });
        }

        const Sensor = Parse.Object.extend("Sensor");
        sensorquery = new Parse.Query(Sensor);
        sensorquery.equalTo("objectId", req.body.sensorid);
        let sensor = await sensorquery.first();

        if (sensor == undefined || sensor == null) {
            return res.status(200).json({ status: "Error", message: "Sensor not found" });
        }

        subscribed_users = sensor.get("subscribedUsers");
        console.log(subscribed_users);
        if (subscribed_users.includes(req.userData.userId)) {
            return res.status(200).json({ status: "Error", message: "User already subscribed" });
        }

        subscribed_users.push(req.userData.userId);
        sensor.set('subscribedUsers', subscribed_users);
        sensor.save()
        res.status(200).json({ status: "Ok", message: "Subscribed to sensor" });
    });

    app.post('/api/sensor/registernotifications', checkAuth, async (req, res, next) => {
        console.log(req.body);
        if (req.userData.user.get("isboard") == undefined || req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Users of type 'board' cannot register for notifications" });
        }
        if (req.body.sensorid == undefined) {
            return res.status(200).json({ status: "Error", message: "Which sensor shall be registered for notifications?" });
        }
        if (req.body.pushToken == undefined) {
            return res.status(200).json({ status: "Error", message: "Can't register for notification without push notification token" });
        }

        const Sensor = Parse.Object.extend("Sensor");
        sensorquery = new Parse.Query(Sensor);
        sensorquery.equalTo("objectId", req.body.sensorid);
        let sensor = await sensorquery.first();

        if (sensor == undefined || sensor == null) {
            return res.status(200).json({ status: "Error", message: "Sensor not found" });
        }

        error_flag = sensorHelper.update_pushToken(req.userData.userId, req.body.pushToken);
        if (error_flag) {
            return res.status(200).json({ status: "Error", message: "User is already registered for notifications" });
        }
        sensor.save()

        return res.status(200).json({ status: "Ok", message: "Registered sensor for notifications" });
    });

    app.post('/api/sensor/setup/set', checkAuth, async (req, res, next) => {
        console.log(`/api/sensor/setup/set: ${JSON.stringify(req.body)}`);
        if (req.userData.user.get("isboard") == undefined || req.userData.user.get("isboard") == true) {
            return res.status(200).json({ status: "Error", message: "Only users can change board setup" });
        }
        if (req.body.sensorSetup == undefined) {
            return res.status(200).json({ status: "Error", message: "sensorSetup missing" });
        }
        if (req.body.sensorSetup.sensorId == undefined) {
            return res.status(200).json({ status: "Error", message: "sensorId undefined." });
        }
        if (req.body.sensorSetup.sensorName == undefined) {
            return res.status(200).json({ status: "Error", message: "sensorName undefined." });
        }
        if (req.body.sensorSetup.upperBound == undefined) {
            return res.status(200).json({ status: "Error", message: "upperBound undefined." });
        }
        if (req.body.sensorSetup.lowerBound == undefined) {
            return res.status(200).json({ status: "Error", message: "lowerBound undefined." });
        }
        if (req.body.sensorSetup.lowerBound == undefined) {
            return res.status(200).json({ status: "Error", message: "lowerBound undefined." });
        }
        if (req.body.sensorSetup.maxNumWarnings == undefined) {
            return res.status(200).json({ status: "Error", message: "maxNumWarnings undefined." });
        }
        if (req.body.sensorSetup.subscribed == undefined) {
            return res.status(200).json({ status: "Error", message: "subscribed undefined." });
        }
        if (req.body.sensorSetup.notified == undefined) {
            return res.status(200).json({ status: "Error", message: "notified undefined." });
        }
        if (req.body.sensorSetup.sensorType == undefined) {
            return res.status(200).json({ status: "Error", message: "sensorType undefined." });
        }
        if (req.body.sensorSetup.unit == undefined) {
            return res.status(200).json({ status: "Error", message: "unit undefined." });
        }
        if (req.body.pushToken == undefined) {
            return res.status(200).json({ status: "Error", message: "pushToken undefined." });
        }

        const Sensor = Parse.Object.extend("Sensor");
        sensorquery = new Parse.Query(Sensor);
        sensorquery.equalTo("objectId", req.body.sensorSetup.sensorId);
        let sensor = await sensorquery.first();

        console.log(`org. sensor: ${JSON.stringify(sensor)}`);

        sensor.set('name', req.body.sensorSetup.sensorName);
        sensor.set('unit', req.body.sensorSetup.unit);
        sensor.set('upperBound', req.body.sensorSetup.upperBound);
        sensor.set('lowerBound', req.body.sensorSetup.lowerBound);
        sensor.set('maxNumWarnings', req.body.sensorSetup.maxNumWarnings);



        if (req.body.sensorSetup.subscribed) {
            let subscribed_users = sensor.get("subscribedUsers");
            if (!subscribed_users.includes(req.userData.userId)) {
                subscribed_users.push(req.userData.userId);
                sensor.set('subscribedUsers', subscribed_users);
            }
        } else {
            let subscribed_users = sensor.get('subscribedUsers');
            if (subscribed_users.includes(req.userData.userId)) {
                for (let i = 0; i < subscribed_users.length; i++) {
                    if (subscribed_users[i] === req.userData.userId) {
                        subscribed_users.splice(i, 1);
                    }
                }
                sensor.set('subscribedUsers', subscribed_users);
            }
        }


        if (req.body.sensorSetup.notified) {
            if (req.body.pushToken == undefined) {
                return res.status(200).json({ status: "Error", message: "Can't register for notification without push notification token" });
            } else {
                sensorHelper.update_pushToken(req.userData.userId, req.body.pushToken);
            }

            let notified_users = sensor.get("notifiedUsers");
            if (!notified_users.includes(req.userData.userId)) {
                notified_users.push(req.userData.userId);
                sensor.set('notifiedUsers', notified_users);
            }
        } else {
            let notified_users = sensor.get("notifiedUsers");
            if (notified_users.includes(req.userData.userId)) {
                for (let i = 0; i < notified_users.length; i++) {
                    if (notified_users[i] === req.userData.userId) {
                        notified_users.splice(i, 1);
                    }
                }
                sensor.set('notifiedUsers', notified_users);
            }
        }

        sensor.save();
        console.log(`new sensor: ${JSON.stringify(sensor)}`);
        return res.status(200).json({ status: "Ok", message: "Sensor-setup saved" });

    });

    app.post('/api/sensor/data/get', checkAuth, async (req, res, next) => {
        if (req.body.sensorId == undefined) {
            return res.status(200).json({ status: "Error", message: "Which sensor data do you want?" });
        }
        if (req.body.timespan == undefined) {
            return res.status(200).json({ status: "Error", message: "For which timespan do you want sensor data?" });
        }

        const Measurement = Parse.Object.extend("Measurement");
        const Sensor = Parse.Object.extend("Sensor");
        measurementquery = new Parse.Query(Measurement);
        measurementquery.equalTo("sensorid", Sensor.createWithoutData(req.body.sensorId));
        measurementquery.ascending("createdAt");
        measurementquery.limit(100000);

        if(req.body.timespan>0) {
            let threshold_date = new Date();
            threshold_date = new Date(threshold_date.getTime() - req.body.timespan * 24 * 60 * 60 * 1000);
            console.log(`/api/sensor/data/get: timespane: ${req.body.timespan}, threshold-data: ${threshold_date}`);
            measurementquery.greaterThan("createdAt", threshold_date);
        } else {
            console.log(`/api/sensor/data/get: timespane: unlimited.`);
        }

        const measurements = await measurementquery.find();

        let all_values = [];
        let all_dates = [];
        for (let i = 0; i < measurements.length; i++) {
            all_dates.push(measurements[i].get('createdAt'));
            all_values.push(measurements[i].get('value'));
        }

        const max_data = 1000;
        const trigger = all_dates.length / max_data;

        if (trigger < 1) {
            return res.status(200).json({ status: "Ok", data: { dates: all_dates, values: all_values } });

        } else {
            let values = [];
            let dates = [];

            for(let i = 0; i < all_dates.length; i++) {
                if( i % trigger < 1) {
                    dates.push(all_dates[i]);
                    values.push(all_values[i]);
                }
            }

            return res.status(200).json({ status: "Ok", data: { dates: dates, values: values } })
        }
    });
}
