const pushNotes = require('../push-notifications');

module.exports = (app) => {

	/** @brief ...*/
	app.post('/api/dev1', async (req, res, next) => {
		console.log(`/api/dev1 called> req: ${JSON.stringify(req.body)}`);
		validateSensorValues();
		res.status(200).json({messsage: "Ok"});
	});

	/** @brief API to send custome messages to (all) users. */
	app.post('/api/sendpush', async(req, res, next) => {
		console.log(`/api/sendpush called> req: ${JSON.stringify(req.body)}`);

		try {
			pushNotes.sendPushNotification('Moin!');
			res.status(200).json({status: "Ok", message: "Push notifications send."});
		} catch (e) {
			res.status(400).json({status: "Error", message: e});
		}
	});

};
