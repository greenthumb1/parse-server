const Parse = require('parse/node');
const datahelper = require('../data-helper');
const checkAuth = require('../check-auth');
const pushNotify = require('../push-notifications');


module.exports = (app) => {

    /** @brief Receive new sensor values */
    app.post('/api/data', checkAuth, async (req, res, next) => {
        if (req.userData.user.get("isboard") != true) {
            return res.status(400).json({ status: "Error", message: "User is not a board" });
        }

        //Save incoming data
        let board = await datahelper.find_board_of_user(req.userData.userId);
        let exceeded_measurement_info = [];

        anlg_data = req.body.anlg_data;
        for (var i = 0; i < anlg_data.length; i++) {
            let sensor = await datahelper.find_sensor("ANALOG", i, board.id)
            if (sensor == undefined) {
                sensor = await datahelper.create_sensor("ANALOG", i, board.id);
            }
            datahelper.save_measurement(anlg_data[i], sensor.id);
            const m = datahelper.check_measurement(anlg_data[i], sensor);
            if (m.exceeded) {
                exceeded_measurement_info.push(m);
            }
        }

        dgt_data = req.body.dgt_data;
        for (var i = 0; i < dgt_data.length; i++) {
            let sensor = await datahelper.find_sensor("DIGITAL", i, board.id);

            if (sensor == undefined) {
                sensor = await datahelper.create_sensor("DIGITAL", i, board.id);
            }

            datahelper.save_measurement(dgt_data[i], sensor.id);
            const m = datahelper.check_measurement(dgt_data[i], sensor);
            if (m.exceeded) {
                exceeded_measurement_info.push(m);
            }
        }

        pushNotify.send_pushnotification(exceeded_measurement_info);
        return res.status(200).json({ status: "Ok", message: "Data saved" });
    });


    /** @brief Get current board and sensor information. */
    app.post('/api/data/currentBoards', checkAuth, async (req, res, next) => {
        console.log(`POST /api/data/currentBoards> req: ${JSON.stringify(req.body)}`);
        console.log(`POST /api/data/currentBoards> userData: ${JSON.stringify(req.userData)}`);

        //FIND SUBSCRIBED BOARDS:
        const subscriptionQuery = new Parse.Query("Subscriptions");
        subscriptionQuery.equalTo("userId", req.userData.userId);
        subscriptions = await subscriptionQuery.first();

        var currentBoards = [];
        //FIND INDIVIDUAL BOARDS:
        const query = new Parse.Query("Measurements");
        query.containedIn("board", subscriptions.get('boardSubscriptions')); // TODO Check if subscribed boards must be converted to pointers.
        const boards = await query.distinct("board"); // res: [{"__type":"Pointer","className":"_User","objectId":"Gf9sRWi9Cb"}, ]

        for (var i = 0; i < boards.length; i++) {

            // FIND FOR EVERY BOARD THE CURRENT VALUES:
            query.descending("createdAt");
            query.equalTo("board", boards[i]);
            const boardDataI = await query.first();

            const boardSettingsQuery = new Parse.Query("BoardSettings");
            boardSettingsQuery.equalTo('boardId', boards[i]);
            const boardSetting = await boardSettingsQuery.first();


            boardDataI.set("description", boardSetting.get("description"));
            boardDataI.set("boardName", boardSetting.get("boardName"));

            if (boardDataI.get("analogValues")) {
                let analogSensorsSettings = boardSetting.get("analogSensorsSettings");
                analogSensorsSettings.length = boardDataI.get("analogValues").length;
                boardDataI.set("analogSensorsSettings", analogSensorsSettings);
            }

            if (boardDataI.get("digitalValues")) {
                let digitalSensorsSettings = boardSetting.get("digitalSensorsSettings");
                digitalSensorsSettings.length = boardDataI.get("digitalValues").length;
                boardDataI.set("digitalSensorsSettings", digitalSensorsSettings);
            }

            currentBoards.push(boardDataI);
        }
        return res.status(200).json({ status: "Ok", message: "Data ok", data: currentBoards });
    })


    /** @brief Update information of the current boards */
    app.put('/api/data/currentBoards', async (req, res, next) => {
        console.log(`PUT /api/data/currentBoards> req: ${JSON.stringify(req.body)}`);
        const userQuery = new Parse.Query(Parse.User);

        try {
            let boards = Object.keys(req.body).
                map(
                    key => req.body[key]
                );
            console.log(`boards: ${boards}`);
            for (let i = 0; i < boards.length; i++) {
                userQuery.equalTo("objectId", boards[i].boardId);
                user = await userQuery.first();

                const boardSettingsQuery = new Parse.Query("BoardSettings");
                boardSettingsQuery.equalTo("boardId", user);
                let boardSettings = await boardSettingsQuery.first();
                console.log(`boardSettings: ${boardSettings}`);

                if (boardSettings) {
                    console.log(`boards[i].analogSensorsSettings.length: ${boards[i].analogSensorsSettings.length}`);
                    if (boards[i].analogSensorsSettings.length > 0) {
                        let analogSensorsSettings = boardSettings.get("analogSensorsSettings")
                        analogSensorsSettings.splice(0, boards[i].analogSensorsSettings.length, ...boards[i].analogSensorsSettings);
                        boardSettings.set("analogSensorsSettings", analogSensorsSettings);
                    }

                    console.log(`boards[i].digitalSensorsSettings.length ${boards[i].digitalSensorsSettings.length}`);
                    if (boards[i].digitalSensorsSettings.length > 0) {
                        let digitalSensorsSettings = boardSettings.get("digitalSensorsSettings");
                        digitalSensorsSettings.splice(0, boards[i].digitalSensorsSettings.length, ...boards[i].digitalSensorsSettings);
                        boardSettings.set("digitalSensorsSettings", digitalSensorsSettings);
                    }

                    boardSettings.set("boardName", boards[i].boardName);
                    boardSettings.set("description", boards[i].description);
                    console.log(`new boardSettings: ${JSON.stringify(boardSettings)}`);
                    boardSettings.save();
                } else {
                    return res.status(400).json({ status: "error", "message": "Board settings not found" });
                }
            }
            res.status(200).json({ status: "Ok" });
        } catch (error) {
            res.status(400).json({ status: "Error", message: `Unknown error: ${error}` });
        }
    });


    /** @brief Returns the full history of a single sensor. */
    app.post('/api/data/sensorHistory', async (req, res, next) => {
        console.log(`POST /api/data/sensorHistory> req: ${JSON.stringify(req.body)}`);

        if (req.body.boardId == null || req.body.sensorType == null || req.body.sensorIdx == null) {
            res.status(201).json({ status: 'Error', message: 'Missing parameters (boardId|sensorType|sensorIdx)' });
        }

        try {
            const userQuery = new Parse.Query(Parse.User);
            userQuery.equalTo("objectId", req.body.boardId);
            const user = await userQuery.first()

            const query = new Parse.Query("Measurements");
            query.equalTo('board', user)
            let ref_time = new Date()
            if (req.body.durationInDays != null) {
                ref_time.setDate(ref_time.getDate() - req.body.durationInDays);
            } else {
                // Default duration is one week
                console.log('Set default duration of 1 week.')
                ref_time.setDate(ref_time.getDate() - 7);
            }
            query.greaterThan('createdAt', ref_time)
            query.limit(24 * 365)
            const measurements = await query.find();

            const n_measurements = Number(measurements.length);

            let sensor_history = [];
            let data;
            for (let i = 0; i < n_measurements; i++) {

                // Choose correct sensor type
                if (req.body.sensorType == 'analog' && measurements[i].get('analogValues')) {
                    data = measurements[i].get('analogValues');
                }
                else if (req.body.sensorType == 'digital' && measurements[i].get('digitalValues')) {
                    data = measurements[i].get('digitalValues');
                }

                // .. and ad it to response data
                if (data) {
                    sensor_history.push({ 'timestamp': measurements[i].updatedAt, 'sensor_value': data[req.body.sensorIdx] });
                }
            }

            return res.status(200).json({ status: "Ok", data: sensor_history });

        } catch (error) {
            return res.status(200).json({ status: "Error", message: `Unexpected error: ${error}` });
        }

    });

    /** @brief Well, this function does everything what trolls do. ;-) */
    app.post('/api/data/troll', async (req, res, next) => {
        console.log("Let's troll!");
        const ParseTroll = Parse.Object.extend("Troll");
        const parseTroll = new ParseTroll();

        let a = [];
        for (let i = 0; i <= 7; i++) {
            a.push(new SensorSettings(`analogValue${i}`, 10000, 0));
        }
        parseTroll.set('analogSensorsSettings', JSON.stringify(a));

        let d = [];
        for (let i = 1; i <= 7; i++) {
            d.push(new SensorSettings(`Value${i}`, 10000, 0));
        }
        parseTroll.set('digitalSensorsSettings', JSON.stringify(d));

        parseTroll.save()
        res.status(200).json({ status: "Ok", message: "Data ok", data: parseTroll })
    })
}
