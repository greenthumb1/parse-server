const Parse = require('parse/node');
const jwt = require("jsonwebtoken");
var fs = require('fs');

const SensorSettings = require('../models/sensor-settings');
const checkAuth = require('../check-auth');

module.exports = (app) => {

	app.post('/api/user/signin', async (req, res, next) => {
		// Request output:
		console.log(`/api/users/signin> req: ${JSON.stringify(req.body)}`);

		// Catch known errors:
		if(req.body.username==null){
			return res.status(400).json({status: "Error", message: "User name required"});

		}
		if(req.body.password==null){
			return res.status(400).json({status: "Error", message: "Password required"});
		}
		if(req.body.email==null){
			return res.status(400).json({status: "Error", message: "Email required"});
		}

		// Handle request:
		let user = new Parse.User();
		user.set("username", req.body.username);
		user.set("password", req.body.password);
		user.set("email", req.body.email);
		user.set("verified", false);
		user.set("config", null);
		user.set("isboard", false);

		try {
			await user.signUp();
		} catch (e) {
			return res.status(400).json({status: "Error", message: e.message});
		}

		const Enduser = Parse.Object.extend("Enduser");
		let enduser = new Enduser();
		enduser.set("pushtoken", "");
		enduser.set("userid", null);
		await enduser.save();

		user.set("settings", Enduser.createWithoutData(enduser.id));
		await user.save(null, {useMasterKey: true});

		enduser.set("userid", Parse.User.createWithoutData(user.id));
		enduser.save()

		const token = jwt.sign({email: user.get("email"), userId: user.id}, server_config['jsonWebTokenKey'], {expiresIn: "10y"}); // Optional add {expiresIn: "1h"}

		//Respond to client:
		console.log(`New user created: email: ${user.get("email")}, username: ${user.get("username")}`);
		return res.status(201).json({ userData: {
			status: "Ok",
			message: "User created",
			token: token,
			userId: user.id,
			email: user.get("email"),
			username: user.get("username"),
		}});
	});



	app.post('/api/user/login', async (req, res, next) => {
		// Request output:
		console.log(`/api/users/login> req: ${JSON.stringify(req.body)}`);

		// Catch known errors:
		if(req.body.username==null){
			res.status(400).json({status: "Error", message: "User name required"});
			return;
		}
		if(req.body.password==null){
			res.status(400).json({status: "Error", message: "Password required"});
			return;
		}

		// Handle request:
		try{
			const user = await Parse.User.logIn(req.body.username, req.body.password);
			const token = jwt.sign({email: user.get("email"), userId: user.id}, server_config['jsonWebTokenKey'], {expiresIn: "10y"}); // Optional add {expiresIn: "1h"}

			//Respond to client:
			return res.status(200).json({ userData: {
				status: "Ok",
				message: "User logged in",
				token: token,
				userId: user.id,
				email: user.get("email"),
				username: user.get("username"),
			}});
		} catch(e) {
			return res.status(400).json({status: "Error", message: "Error: " + e.code + " " + e.message});
		}
	});

	app.post('/api/users/pushtoken/update', async (req, res, next) => {
		// Request output:
		console.log(`/api/users/pushtoken/update> req: ${JSON.stringify(req.body)}`);

		// Catch known errors:
		if(req.body.username==null){
			res.status(400).json({status: "Error", message: "User name required"});
			return;
		}
		if(req.body.pushtoken==null){
			res.status(400).json({status: "Error", message: "Push token required"});
			return;
		}

		// Handle request:
		try {
			const query = new Parse.Query("Enduser");
			query.equalTo("userName", req.body.username);
			const userpushtoken = await query.first();

			if(userpushtoken) {
				console.log(`User found in PushTokens. Update entry...`);

				userpushtoken.set("pushToken", req.body.pushToken);
				userpushtoken.save();
			}
			else {
				console.log(`User not found in PushTokens. Create new entry...`);

				const PushTokens = Parse.Object.extend("PushTokens");
				newuserpushtoken = new PushTokens();
				newuserpushtoken.set("pushToken", req.body.pushToken);
				newuserpushtoken.set("userName", req.body.userName);
				newuserpushtoken.save();
			}

			//Respond to client:
			return res.status(200).json({status: "Ok", message: "Push token updated."})
		}catch (e) {
			return res.status(400).json({status: "Error", message: "Error: " + e.code + " " + e.message});
		}
	});
//
//
	//app.put('/api/users/subscription/get', checkAuth, async (req, res, next) => {
			//// Request output:
			//console.log(`PUT /api/users/subscription/add called> req: ${JSON.stringify(req.body)}`);
			//console.log(`PUT /api/users/subscription/add> userData: ${JSON.stringify(req.userData)}`);
//
			//const subscriptionQuery = new Parse.Query("Subscriptions");
			//subscriptionQuery.equalTo('userId', req.userData.userId);
			//const subscription = await subscriptionQuery.first();
			//console.log(`subscription: ${JSON.stringify(subscription)}`);
//
			//let boardIds = subscription.get('boardSubscriptions');
			//let subscribedBoards = [];
			//userQuery = new Parse.Query(Parse.User);
//
			//for (let i=0; i<boardIds.length; i++){
				//console.log(`boardIds[i]: ${boardIds[i]}`);
				//userQuery.equalTo('objectId', boardIds[i]);
				//const board = await userQuery.first();
//
				//const boardSettingsQuery = new Parse.Query("BoardSettings");
				//boardSettingsQuery.equalTo('boardId', board);
				//const boardSetting = await boardSettingsQuery.first();
//
				//subscribedBoards.push({boardId: boardIds[i], boardName: boardSetting.get('boardName')});
			//}
//
			//res.status(200).json({status: "Ok", data: subscribedBoards});
		//});
//
	//app.put('/api/users/subscription/add', checkAuth, async (req, res, next) => {
		//// Request output:
		//console.log(`PUT /api/users/subscription/add called> req: ${JSON.stringify(req.body)}`);
		//console.log(`PUT /api/users/subscription/add> userData: ${JSON.stringify(req.userData)}`);
		//// Catch known errors:
		//if(req.body.boardId==null){
			//res.status(400).json({status: "Error", message: "Error: Required data boardId is missing."});
		//}
//
		//// Handle request:
		//try {
			//// Check if the board exists:
			//const boardQuery = new Parse.Query(Parse.User);
			//boardQuery.equalTo('objectId', req.body.boardId);
			//let board = await boardQuery.first();
			//if (board==null || board.length==0){
				//return res.status(400).json({status: "Error", message: "Error: Board with this ID doesn't exists."});
			//} else {
				//console.log(`Board found.`);
			//}
//
			//// Append the board to users subscriptions if it doesn't exist jet.
			//const subscriptionQuery = new Parse.Query("Subscriptions");
			//subscriptionQuery.equalTo('userId', req.userData.userId);
			//const subscription = await subscriptionQuery.first();
//
			//if(subscription==null){
				//return res.status(400).json({status: "Error", message: "Subscription for user doesn't exist. DATABASE Error!"})
			//} else {
				//console.log(`Subscription found: ${JSON.stringify(subscription)}`);
			//}
//
			//let entries = subscription.get('boardSubscriptions')
//
			//let boardSubscriptionExists = false;
			//for(let i=0; i<entries.length; i++){
				//if(entries[i]==req.body.boardId){
					//boardSubscriptionExists = true;
				//}
			//}
//
			//if(!boardSubscriptionExists){
				//entries.push(req.body.boardId);
			//}
//
			//subscription.set('boardSubscriptions', entries);
			//subscription.save();
//
//
			//const Notifications = Parse.Object.extend("Notifications");
			//const notifications = new Notifications();
			//const User = Parse.Object.extend("User");
			//notifications.set("userId", User.createWithoutData(req.userData.userId));
			//notifications.set("boardId", User.createWithoutData(req.body.boardId));
			//let lan = [];
			//for(let i=0; i<=7; i++) {
				//lan.push(new Date(0));
			//}
			//notifications.set("lastAnalogNotification", lan);
				//let ldn = [];
			//for(let i=0; i<=7; i++) {
				//ldn.push(new Date(0));
			//}
			//notifications.set("lastDigitalNotification", ldn);
			//notifications.save();
//
			//if(!boardSubscriptionExists){
			//return res.status(200).json({status: "Ok", message: "Board subscription added."});
			//} else {
				//return res.status(202).json({status: "Ok", message: "Board is already subscribed."})
			//}
//
		//}catch (e) {
			//return res.status(400).json({status: "Error", message: "Error: " + e.code + " " + e.message});
		//}
//
	//});
//
//
	//app.put('/api/users/subscription/delete', checkAuth, async (req, res, next) => {
		//// Request output:
		//console.log(`PUT /api/users/subscription/add called> req: ${JSON.stringify(req.body)}`);
		//console.log(`PUT /api/users/subscription/add> userData: ${JSON.stringify(req.userData)}`);
		//// Catch known errors:
		//if(req.body.boardIds==null){
			//res.status(400).json({status: "Error", message: "Error: Required data boardIds is missing."});
		//}
//
//
		//const subscriptionQuery = new Parse.Query("Subscriptions");
		//subscriptionQuery.equalTo('userId', req.userData.userId);
		//const subscription = await subscriptionQuery.first();
		//console.log(`subscription: ${JSON.stringify(subscription)}`);
		//let subscibedBoards = subscription.get('boardSubscriptions');
//
		//for(let i=0; i<req.body.boardIds.length; i++){
			//for(let j=subscibedBoards.length-1; j>=0; j--){
				//if(subscibedBoards[j]==req.body.boardIds[i]) {
					//subscibedBoards.splice(j,1);
				//}
			//}
		//}
//
		//subscription.set('boardSubscriptions', subscibedBoards);
		//subscription.save();
		//return res.status(200).json({status: 'Ok', message: 'Board subscriptions deleted if existed.'});
	//});
}
