const express = require('express');
var http = require('http');
var https = require('https');
const ParseServer = require('parse-server').ParseServer;
const fs = require('fs');
const bodyParser = require('body-parser');
const helper = require('./helper');

// CONFIG:
const port = 1337;
server_config = JSON.parse(fs.readFileSync('server-config.json', 'utf8'));

// EXPRESS:
var app = express();
app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Parse-Application-Id");
	res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS");
	next();
});
app.use(bodyParser.json()); //middleware that parse the http bodies of all incoming requests. next() is called automatically.
app.use(bodyParser.urlencoded({extended: false})); // Like above just for all variables wich are saved in the url

require('./routes/data')(app);
require('./routes/users')(app);
require('./routes/boards')(app);
require('./routes/sensors')(app);
require('./routes/dev')(app);

// PARSE:
const parse_config = JSON.parse(fs.readFileSync('parse-config.json', 'utf8'));
var api = new ParseServer(parse_config);
app.use('/parse', api);

try {
	var localip = helper.get_localIP('wlp3s0');
} catch (e) {
	var localip = 'localhost'
}
http.createServer(app).listen(server_config.port, function() {
  console.log(`SmartHome-Server running on http://${localip}:${server_config.port}.`);
});
