const localip = require('local-ip');

module.exports = {

  get_localIP: function(iface) {
    var ip =localip(iface, (err, res) => {
      if (err) {
        throw new Error(`Wrong network interface? ${err}`);
      }
      return res;
    });
    return ip;
  }

}
