class SensorSettings {
  constructor(x, y, z, u, r) {
    this.sensorName = x;
    this.upperBound = y;
		this.lowerBound = z;
    this.unit = u;
    this.maxNumWarnings = r;
  }
};

module.exports = SensorSettings;
