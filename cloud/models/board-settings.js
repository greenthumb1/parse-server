
const SensorSettings = require('../models/sensor-settings');

class BoardSettings {
  constructor(a, b, c, d, e) {
		this.userId = a;
    this.boardName = b;
		this.description = c;
    this.analogSensorsSettings = d;
		this.digitalSensorsSettings = e;
  }
};

module.exports = BoardSettings;
