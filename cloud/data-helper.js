
module.exports = {

    find_sensor: async function(type, index, boardid) {
        const Sensor = Parse.Object.extend("Sensor");
        const Board = Parse.Object.extend("Board");
        sensorquery = new Parse.Query(Sensor);
        sensorquery.equalTo("boardid", Board.createWithoutData(boardid));
        sensorquery.equalTo("index", index);
        sensorquery.equalTo("type", type);
        let sensor = await sensorquery.first();

        return sensor;
    },

    find_board_of_user: async function(userid) {
        const Board = Parse.Object.extend("Board");
        boardquery = new Parse.Query(Board);
        boardquery.equalTo("userid", Parse.User.createWithoutData(userid));
        let board = await boardquery.first();

        return board;
    },

    create_sensor: async function(type, index, boardid) {
        console.log(`create new sensor: ${boardid}, ${type}, ${index}`);
        const Board = Parse.Object.extend("Board");
        const Sensor = Parse.Object.extend("Sensor");
        let sensor = new Sensor();
        sensor.set("name", "new sensor");
        sensor.set("type", type);
        sensor.set("index", index);
        sensor.set("boardid", Board.createWithoutData(boardid));
        sensor.set("upperBound", 1000);
        sensor.set("lowerBound", 0);
        sensor.set("unit", "1");
        sensor.set("maxNumWarnings", -1);
        sensor.set("lastNotified", new Date(0));
        sensor.set("notifiedUsers", []);
        sensor.set("subscribedUsers", []);
        await sensor.save()

        return sensor;

    },

    save_measurement: async function (value, sensorid) {
        const Sensor = Parse.Object.extend("Sensor");
        const Measurement = Parse.Object.extend("Measurement");
        measurement = new Measurement();
        measurement.set("value", value);
        measurement.set("sensorid", Sensor.createWithoutData(sensorid));
        await measurement.save();
    },

    check_measurement: function(value, sensor) {
        let exceed_info = {exceeded: false};

        if(value<sensor.get("lowerBound")) {
            exceed_info = {exceeded: true,
                           sensor: sensor,
                           toolow: true,
                           value: value,
                          };
        }

        else if(value>sensor.get("upperBound")) {
            exceed_info = {exceeded: true,
                           sensor: sensor,
                           toolow: false,
                           value: value,
                          };
            }

        return exceed_info;
    }

}
