
const Parse = require('parse/node');

module.exports = {

    async update_pushToken(userid, pushtoken) {

        const Enduser = Parse.Object.extend("Enduser");
        enduserquery = new Parse.Query("Enduser");
        const User = Parse.Object.extend("User");
        enduserquery.equalTo("userid", User.createWithoutData(userid));
        let enduser = await enduserquery.first();
        console.log(enduser);

        enduser.set("pushtoken", pushtoken);
        enduser.save();
    }

}
