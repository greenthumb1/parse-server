const Parse = require("parse/node");

function find_sensors_for_user_mesage(user_index, exceeded_measurement_info) {
  let sensor_warnings = [];
  for (let i = 0; i < exceeded_measurement_info.length; i++) {
    const sensor = exceeded_measurement_info[i].sensor;
    console.log(`maxNumWarnings: ${sensor.get("maxNumWarnings")}`);
    if (sensor.get("maxNumWarnings") > 0) {
      const time_next_sensor_notification =
        sensor.get("lastNotified").getTime() +
        (24 * 60 * 60 * 1000) / sensor.get("maxNumWarnings");
      const now = new Date().getTime();

      if (
        exceeded_measurement_info[i].sensor.get("notifiedUsers") ==
          user_index &&
        time_next_sensor_notification < now
      ) {
        sensor_warnings.push(exceeded_measurement_info[i]);
      }
    }
  }
  return sensor_warnings;
}

function create_message(sensor_warnings) {
  console.log(`sensor_warnings: ${sensor_warnings.length}`);
  let message = "";
  if (sensor_warnings.length == 0) {
  } else if (sensor_warnings.length > 1) {
    message = " Multiple sensors have exceeded their limits.";
  } else {
    let unit = sensor_warnings[0].sensor.get("unit");
    let sensorname = sensor_warnings[0].sensor.get("name");
    let limit = sensor_warnings[0].sensor.get("lowerBound");
    let value = sensor_warnings[0].value;
    let toolow = sensor_warnings[0].toolow;
    if (toolow) {
      message = `'${sensorname}' has exceeded it's lower limit by ${sensor_warnings[0].sensor.get(
        "lowerBound"
      ) - value} \[${unit}\].`;
    } else {
      message = `'${sensorname}' has exceeded it's upper limit by ${value -
        sensor_warnings[0].sensor.get("upperBound")} \[${unit}\].`;
    }
  }
  return message;
}

function create_exceeding_info(sensor_warnings) {
  let exceeding_info = [];
  for (let i = 0; i < sensor_warnings.length; i++) {
    exceeding_info.push({
      boardid: sensor_warnings[0].sensor.get("boardid").id,
      sensorid: sensor_warnings[0].sensor.id
    });
  }
  return exceeding_info;
}

module.exports = {
  find_user_to_notify: async function(exceeded_measurement_info) {
    let collected_users = [];
    for (let i = 0; i < exceeded_measurement_info.length; i++) {
      const notified_users = exceeded_measurement_info[i].sensor.get(
        "notifiedUsers"
      );
      for (let j = 0; j < notified_users.length; j++) {
        if (!collected_users.includes(notified_users[j])) {
          collected_users.push(notified_users[j]);
        }
      }
    }
    return collected_users;
  },

  create_push_data_for_user: async function(
    user_index,
    exceeded_measurement_info
  ) {
    let push_data = { message: "", exceeding_info: [] };
    const sensor_warnings = await find_sensors_for_user_mesage(
      user_index,
      exceeded_measurement_info
    );
    push_data.message = await create_message(sensor_warnings);
    push_data.exceeding_info = await create_exceeding_info(sensor_warnings);
    return push_data;
  },

  get_pushtoken: async function(userid) {
    const Enduser = Parse.Object.extend("Enduser");
    const enduserquery = new Parse.Query(Enduser);
    const User = Parse.Object.extend("User");
    enduserquery.equalTo("userid", User.createWithoutData(userid));
    const enduser = await enduserquery.first();
    const pushtoken = enduser.get("pushtoken");

    return pushtoken;
  },

  save_sensors_lastNotified: async function(push_data, delay) {
    // NOTE: some delay to ensure that lastNotified is not updated before all user notiifications are send.
    setTimeout(async () => {
      for (let s = 0; s < push_data.exceeding_info.length; s++) {
        const Sensor = Parse.Object.extend("Sensor");
        sensorquery = new Parse.Query(Sensor);
        sensorquery.equalTo("objectId", push_data.exceeding_info[s].sensorid);
        let sensor = await sensorquery.first();
        sensor.set("lastNotified", new Date());
        sensor.save();
      }
    }, delay);
  }
};
