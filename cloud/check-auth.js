const jwt = require("jsonwebtoken");
const util = require('util')

module.exports = async (req, res, next) => {
	//console.log(`AUTHGUARD> req: ${JSON.stringify(req.body)}`);
	if(req.body.authorization==undefined || req.body.authorization==null) {
		return res.status(401).json({status: "error", message: "Authorization token required"});
	}

	try{
		const token = req.body.authorization;
		const decodedToken = jwt.verify(token, server_config['jsonWebTokenKey']);
		// console.log(`decodedToken: ${util.inspect(decodedToken, {showHidden: false, depth: null})}`);
		req.userData = {userId: decodedToken.userId};

		const User = Parse.Object.extend("User");
		userquery = new Parse.Query(User);
		userquery.equalTo("objectId", req.userData.userId);
		const user = await userquery.first();

		if((user.get("emailVerified")==null) || (user.get("emailVerified")==false)){
			return res.status(402).json({status: "Error", message: "Error: User email needs to be verfied by system admin."});
		}
		if((user.get("verified")==null) || (user.get("verified")==false)){
			return res.status(402).json({status: "Error", message: "Error: User needs to be verfied by system admin."});
		}

		req.userData = {userId: decodedToken.userId, user: user};
		delete req.body["authorization"];
		next();
	} catch (e) {
		return res.status(401).json({status: "error", message: "Auth failed", error: e})
	}
};
