const Parse = require("parse/node");
const gcm = require("node-gcm");
const helper = require("./pushnotification-helper");

module.exports = {
  send_pushnotification: async function(exceeded_measurement_info) {
    const sender = new gcm.Sender(server_config["firebaseServerAPI"]);
    console.log(`firebaseAPI: ${server_config["firebaseServerAPI"]}`);

    collected_users = await helper.find_user_to_notify(
      exceeded_measurement_info
    );
    for (let u = 0; u < collected_users.length; u++) {
      const push_data = await helper.create_push_data_for_user(
        collected_users[u],
        exceeded_measurement_info
      );
      const pushtoken = await helper.get_pushtoken(collected_users[u]);

      if (pushtoken != undefined && push_data.exceeding_info.length != 0) {
        let message = new gcm.Message({
          data: {
            data: push_data.exceeding_info
          },
          notification: {
            title: "Sensor warning",
            icon: "ic_icon_push_notification",
            body: push_data.message,
            color: "#ffce00"
          }
        });

        sender.send(
          message,
          { registrationTokens: [pushtoken] },
          (err, res) => {
            if (err) {
              console.log(`Error during sending push notifications: ${err}`);
            } else {
              console.log("Push notification send.");
              helper.save_sensors_lastNotified(push_data, 1000);
            }
          }
        );
      }
    }
  }
};
