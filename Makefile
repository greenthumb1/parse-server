SERVER_URL = localhost #https://greenthumb-api.ludwigel.de
SERVER_PORT = 1337
SERVER_URL = localhost:1337
# --------------
#USER_NAME =	    ludwig
#USER_EMAIL =    ludwig@test.com
#USER_PASSWORD = abcd1234
#USER_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imx1ZHdpZ0B0ZXN0LmNvbSIsInVzZXJJZCI6Im5nUm9uTUE4UG8iLCJpYXQiOjE2Mjc4NjA2NTcsImV4cCI6MTk0MzQzNjY1N30.m9TlMP1uqCb84JpCgdz_dsLSN53ZkUZ-548xDcMqMWc
# --------------
USER_NAME = test-board-neo
USER_PASSWORD = abcd1234
USER_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJ1emtvMVdjY0NMIiwiaWF0IjoxNjEwMjc0Njg2LCJleHAiOjE5MjU4NTA2ODZ9.f5txmZNpPshnPIvU5vPXFIcLHhrayCYkCGxhJNzYf9s
SENSOR_ID = Ez72HBiaim
TIMESPAN = 68

01_start_database:
	mongod --config mongodb/mongod.conf

02_start_server:
	nodemon start

03_dashboard:
	parse-dashboard --dev --appId smartHomeServerId --masterKey smartHomeServerMasterKey --serverURL "http://localhost:1337/parse" --appName SmartHomeServer

connect:
	ssh -p 22 ludwig@${SERVER_IP}

create_enduser:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"name":"${USER_NAME}","email":"${USER_EMAIL}","password":"${USER_PASSWORD}"}' \
	${SERVER_URL}/api/user/signin

create_board:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"username":"${USER_NAME}","password":"${USER_PASSWORD}"}' \
	${SERVER_URL}/api/board/register

login_user:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"username":"${USER_NAME}","password":"${USER_PASSWORD}"}' \
	${SERVER_URL}/api/user/login

login_board:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"username":"${USER_NAME}","password":"${USER_PASSWORD}"}' \
	${SERVER_URL}/api/board/login

post_data:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"anlg_data":[12,10.333333333333,10],"authorization":"${USER_TOKEN}","version":"1.0.0","dgt_data":[99,99,54612.5],"ip":"192.168.178.93"}' \
	${SERVER_URL}/api/data

subscribe_sensor:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"sensorid":"BTBFDE2Ddu","authorization":"${USER_TOKEN}"}' \
	${SERVER_URL}/api/sensor/subscribe

register_sensor_notifications:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"sensorid":"${SENSOR_ID}","authorization":"${USER_TOKEN}","pushtoken":"someC00lToken"}' \
	${SERVER_URL}/api/sensor/registernotifications

subscribe_board:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"boardid":"yLUUXuSDlI","authorization":"${USER_TOKEN}"}' \
	${SERVER_URL}/api/board/subscribe

get_boards_setup:
	curl -X GET -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"authorization":"${USER_TOKEN}"}' \
	${SERVER_URL}/api/boards/setup

get_sensor_data:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"authorization":"${USER_TOKEN}", "sensorId": "${SENSOR_ID}", "timespan": "${TIMESPAN}"}' \
	${SERVER_URL}/api/sensor/data/get


#-----------------------------------------------------------------------------
put_currentBoardInfo:
	curl -X PUT -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"test": "blabla"}' \
	${SERVER_URL}/api/data/currentBoards

sendpush:
	curl -X POST -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	$(SERVER_URL)/api/sendpush

get_sensorHistory:
	curl -X GET -H "Content-Type: application/json" \
	-H "X-Parse-Application-Id: smartHomeServerId" \
	-d '{"boardId": "Gf9sRWi9Cb","sensorType":"digital","sensorIdx":0}' \
	$(SERVER_URL)/api/data/sensor_history
